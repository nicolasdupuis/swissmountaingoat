import plotly.graph_objects as go
import plotly.express as px
import pandas as pd

class Figures:

    def __init__(self, places: pd.DataFrame, activities: pd.DataFrame):
    
        self.places = places
        self.activities = activities

        # Colors
        self.waypoint_types_color = {'Sommet':  'brown',
                                     'Col':     'darkOrange',
                                     'Refuge':  'darkGreen',
                                     'POI':     'grey',
                                     'Falaise': 'dodgerblue'}

        self.activity_types_color = {'Randonnée':   'forestgreen',
                                     'Alpinisme':   'darkorchid',
                                     'Ski':         'blue', 
                                     'Escalade':    'orangered',
                                     'Vélo':        'darkturquoise',
                                     'Via ferrata': 'darkorange'}

        # Merge Activities and Places
        #------------------------------------------------------------------------------------------------        
        df = self.activities[['Date', 'Waypoints']].dropna()
        df1 = pd.DataFrame(df.Waypoints.str.split(', ').tolist(), index=df.Date).stack()
        df1 = df1.reset_index([0, 'Date'])
        df1.columns = ['Date', 'Waypoints']
        df1['Année'] = df1.Date.dt.year.astype(int)
        self.activity_year = df1.merge(places[['Waypoints', 'Type', 'Altitude', 'Latitude', 'Longitude']], how='left', on='Waypoints').dropna()


        # Days in the mountain, over time and by activity type
        #------------------------------------------------------------------------------------------------
        df = pd.DataFrame(self.activities.groupby(['Année', 'Activité'])['Journées'].sum()).reset_index()

        self.activities_overtime = go.Figure(
            data = [ go.Bar(name        = activity,
                            x           = list(df[df['Activité'] == activity]['Année']),
                            y           = list(df[df['Activité'] == activity]['Journées']),
                           marker_color = self.activity_types_color[activity])
                   
                   for activity in self.activities['Activité'].unique()
                ]
            )

        self.activities_overtime.update_layout(barmode='stack')
        self.activities_overtime.update_layout(yaxis=dict(title='Journées'))

        df = self.activities.groupby(['Année'])['Journées'].sum()
        for year, days in df.items():
            self.activities_overtime.add_annotation(x=year, y=days+3, text=f"{days}", showarrow=False)


        # Waypoints in the mountain, over time and by type
        #------------------------------------------------------------------------------------------------
        df = pd.DataFrame(self.activity_year.groupby(['Année', 'Type'])['Waypoints'].count()).reset_index()

        self.waypoints_overtime = go.Figure(
            data = [ go.Bar(name        = _type,
                            x           = list(df[df['Type'] == _type]['Année']),
                            y           = list(df[df['Type'] == _type]['Waypoints']),
                           marker_color = self.waypoint_types_color[_type])
                   
                   for _type in df['Type'].unique()
                ]
            )                  
            
        self.waypoints_overtime.update_layout(barmode='stack')
        self.waypoints_overtime.update_layout(yaxis=dict(title='Lieux'))


        # Context evolution 
        #------------------------------------------------------------------------------------------------
        def context(row):

            if 'guide' in row['Contexte'] or row['Role'] in ['Second', 'Participant']:
                return 'Encadré' 
            elif row['Role'] in ['Encadrant', 'Premier']:
                return 'Lead' 
            elif row['Role'] in ['Réversible']:
                return 'Réversible' 
            else:
                return 'Solo & N/A'

        activities.fillna('', inplace=True)
        activities['Situation'] = activities.apply(context, axis=1)
        df = pd.DataFrame(activities.groupby(['Année', 'Situation'])['Journées'].sum()).reset_index()

        context_fig = go.Figure(
            data=[ go.Bar(  name='Encadré',    x=list(df[df['Situation']=='Encadré']['Année']),    y=list(df[df['Situation']=='Encadré']['Journées']),    marker_color='orangered'),
                   go.Bar(  name='Solo & N/A', x=list(df[df['Situation']=='Solo & N/A']['Année']), y=list(df[df['Situation']=='Solo & N/A']['Journées']), marker_color='blue'),
                   go.Bar(  name='Réversible', x=list(df[df['Situation']=='Réversible']['Année']), y=list(df[df['Situation']=='Réversible']['Journées']), marker_color='darkorchid'),
                   go.Bar(  name='Lead',       x=list(df[df['Situation']=='Lead']['Année']),       y=list(df[df['Situation']=='Lead']['Journées']),       marker_color='forestgreen'),       
            ])

        context_fig.update_layout(barmode='stack')
        context_fig.update_layout(yaxis=dict(title='Journées'))

        self.context_evolution = context_fig


        # Activities by grades
        #------------------------------------------------------------------------------------------------

        self.climb_by_grades = {}
        for activity in ['Alpinisme', 'Randonnée', 'Escalade', 'Ski']:

            if activity=='Ski': 
                df = self.activities[ (self.activities['Activité'] == activity) & 
                                      (activities['Cotation']     != 'Piste')]
            else: 
                df = self.activities[self.activities['Activité']==activity]
            
            df = df.sort_values(by='Grade')
            fig = go.Figure(
                data = go.Scatter(
                            x            = df['Date'],
                            y            = df['Cotation'],
                            mode         = 'markers',
                            marker_color = self.activity_types_color[activity],
                            text         = df['Nom']
                        )
                )

            total = df.groupby('Cotation').count()
            for idx, row in total.iterrows():
                fig.add_annotation(x=max(df['Date']), y=idx, text=f"({row['Journées']})", showarrow=False, xshift=30 )

            fig.update_layout(title=f'{activity} (' + str(len(df)) + ')')
            self.climb_by_grades[activity] = fig
        

    def update_waypoints_figure(self, altitude_range, types):

        # Apply user's filter on places 
        df = self.places[ (self.places['Altitude'] >= altitude_range[0]) &
                          (self.places['Altitude'] <= altitude_range[1]) & 
                          (self.places['Type'].isin(types))
                ]

        # Stats
        df = df.sort_values('Altitude').reset_index()
        df['index'] = df.index

        fig = go.Figure(
            data = [ go.Scatter(name         = _type,
                                x            = df[df['Type']==_type]['index'],
                                y            = df[df['Type']==_type]['Altitude'],
                                mode         = 'markers',
                                text         = df[df['Type']==_type]['Waypoints'],
                                marker_color = self.waypoint_types_color[_type])

                    for _type in df['Type'].unique()
            ]
        )                            

        fig.update_layout(yaxis=dict(title='Altitude (mètres)'))
        fig.update_xaxes(showticklabels=False)  

        return fig


    def update_waypoints_year_figure(self, altitude_range, types):

        places = self.activity_year[ (self.activity_year['Altitude'] >= altitude_range[0]) &
                                     (self.activity_year['Altitude'] <= altitude_range[1]) & 
                                     (self.activity_year['Type'].isin(types))
                                    ]
       
        figure = go.Figure(
            data = [ go.Scatter(name         = _type,
                                x            = places[places['Type']==_type]['Date'],
                                y            = places[places['Type']==_type]['Altitude'],
                                mode         = 'markers',
                                text         = places[places['Type']==_type]['Waypoints'],
                                marker_color = self.waypoint_types_color[_type])

                    for _type in places['Type'].unique()
            ]
        )   


        return figure