import plotly.express as px
import pandas as pd
from datetime import datetime


def numeric_grading(row):

    grades = {'Alpinisme': {'F': 1,   'F+': 1.4, 'PD-': 1.6, 'PD': 2,  'PD+': 2.4, 'AD-': 2.6, 'AD': 3, 'AD+': 3.4, 'D-': 3.6, 'D': 4, 'D+': 4.4, 'TD-':4.6, 'TD': 5},
            'Via ferrata': {'K1': 1,  'K2': 2,   'K3': 3,    'K4': 4,  'K5': 5},
            'Randonnée':   {'T3': 3,  'T4': 4,   'T4+': 4.5, 'T5': 5,  'T5+': 5.5, 'T6': 6},
            'Ski':         {'PD-': 1, 'PD': 2,   'PD+': 3,   'AD-': 3, 'AD': 4},
    }

    activity = row['Activité']
    grade = row['Cotation']

    try: 
        return grades[activity][grade]
    except:
        return grade


def load_data(csv_file):

    places = pd.read_excel(csv_file, sheet_name='places')
    activities = pd.read_excel(csv_file, sheet_name='courses').sort_values(by=['Date'], ascending=[False])
    activities['Date_str'] = pd.to_datetime(activities['Date']).dt.date
    activities['Grade'] = activities.apply(numeric_grading, axis=1)
    activities['Année'] = activities.Date.dt.year

    return places, activities


def generate_map(figures, named_place, 
                 altitude_values, altitude_range,
                 year_values, year_range,
                 types):

    zoom = 8

    # User searched for that place, let's focus on it
    if named_place:
        record = figures.places[figures.places['Waypoints']==named_place]
        if len(record) == 1:
            places=record
            zoom = 12
    
    else: 

        places = figures.places[figures.places['Type'].isin(types)]

        # Filter on altitude and year, if needed
        if altitude_range != altitude_values:
            places = places[ places['Altitude'].between(altitude_values[0], altitude_values[1], inclusive='both')]

        elif year_values != year_range:
            places = figures.activity_year[figures.places['Type'].isin(types)]
            places = places[ places['Date'].between(datetime(year_values[0], 1, 1),
                                                    datetime(year_values[1] - 1, 12, 31),
                                                    inclusive='both')]

    new_map = px.scatter_mapbox(places,
                                lat                     = "Latitude", 
                                lon                     = "Longitude",
                                color                   = 'Type',
                                color_discrete_map      = {_type: figures.waypoint_types_color[_type] for _type in figures.waypoint_types_color},
                                hover_name              = "Waypoints",
                                hover_data              = ['Altitude'],
                                zoom                    = zoom,
                                height                  = 700)

    mapbox_token = open(".mapbox_token").read()
    new_map.update_layout(mapbox_style='outdoors', mapbox_accesstoken=mapbox_token)

    new_map.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

    return new_map