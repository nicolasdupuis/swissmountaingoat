'''
   Nicolas Dupuis, 2019-2021
'''

import dash
import dash_bootstrap_components as dbc
from dash import html, dcc
from dash.dependencies import Input, Output, State
import pandas as pd
from datetime import datetime
from utilities import generate_map, load_data
from figures import Figures
import dash_tabulator


# ----------------------------------------------------------------------------
# Initialize the app
# ----------------------------------------------------------------------------
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
app.title = "The Mountain Goat"
server = app.server
app.config.suppress_callback_exceptions = True

# ----------------------------------------------------------------------------
# Load data into 2 dataframes: places and activities
# ----------------------------------------------------------------------------
places, activities = load_data(csv_file='data/places.xlsx')

# ----------------------------------------------------------------------------
# Create Plotly figures
# ----------------------------------------------------------------------------
figures = Figures(places, activities)

# ----------------------------------------------------------------------------
# Dash layout
# ----------------------------------------------------------------------------
def layout(figures):

    activities_cols = [
        { "title": "Nom",          "field": "Nom",          "headerFilter":True, "hozAlign": "left", "width":  200},
        { "title": "Activité",     "field": "Activité",     "headerFilter":True, "hozAlign": "left",  "width": 140},
        { "title": "Cotation",     "field": "Cotation",     "headerFilter":True, "hozAlign": "left",  "width": 100},
        { "title": "Date",         "field": "Date_str",     "headerFilter":True, "hozAlign": "left",  "width": 100},
        { "title": "Journées",     "field": "Journées",     "headerFilter":True, "hozAlign": "left",  "width": 100},
        { "title": "Topo",         "field": "Topo/Blog",    "headerFilter":True, "hozAlign": "left",  "width": 110, "formatter": "link"},
        { "title": "Contexte",     "field": "Contexte",     "headerFilter":True, "hozAlign": "left",  "width": 150},
        { "title": "Role",         "field": "Role",         "headerFilter":True, "hozAlign": "left",  "width": 150},
        { "title": "Participants", "field": "Participants", "headerFilter":True, "hozAlign": "left",  "width": 120},
        { "title": "Commentaire",  "field": "Commentaire",  "headerFilter":True, "hozAlign": "left"}]

    stats_places = places.groupby('Type')['Type'].count()

    # Tab content: 'Carte'
    map_content = html.Div([        
        dbc.Card([
            dbc.CardBody([

                dbc.Button("Filtres", id="filter_button", className="mb-3", color="primary", n_clicks=0),

                dbc.Collapse([

                    dbc.Card([

                        dbc.CardBody([

                            dbc.Row([
                                dbc.Col([ html.Label('Lieu') ], width = 1),
                                dbc.Col([ 
                                    dcc.Dropdown(
                                        id          = "search_place", 
                                        placeholder = "Tapez un nom...",
                                        options     = [{"label": place, "value": place} for place in places['Waypoints']]),
                                ], width= 3),
                                dbc.Col([ 
                                    dbc.Checklist(
                                        options     = [{'label': _type, 'value': _type} for _type in places['Type'].unique()],
                                        value       = [ _type for _type in places['Type'].unique()],
                                        id          = "type_selection",
                                        inline      = True,
                                        persistence = True),
                                ], width = 5),
                            ]),
                            html.Br(),

                            dbc.Row([
                                dbc.Col([ html.Label('Altitude') ], width = 1),
                                dbc.Col([ 
                            
                                    dcc.RangeSlider(
                                                id='altitude_slider',
                                                min=0, max=5000,
                                                value=[0, 5000],
                                                marks={0: '0m', 1000: '1000m', 2000: '2000m', 3000: '3000m', 4000: '4000m', 5000: '5000m'},
                                                className="dcc_control")
                                ], width = 10),
                            ]),
                            html.Br(),

                            dbc.Row([
                                dbc.Col([ html.Label('Année') ], width = 1),
                                dbc.Col([ 
                                    dcc.RangeSlider(id='year_slider',
                                                min=2008, 
                                                max=datetime.now().year + 1,
                                                value=[2008, datetime.now().year + 1],
                                                marks={year: f'{year}' for year in range(2008, datetime.now().year + 1)},
                                                className="dcc_control"),
                                ], width = 10),
                            ]),
                        ]),
                    ]),

                ], id ='collapse_filters'),
                html.Br(),

            dcc.Graph(id='map')
            ])
        ])
    ])

    # Tab content: 'Sorties'
    activities_content = html.Div([        
        dbc.Card([
            dbc.CardBody([
                html.Label(id='selection_sorties'), html.Br(), html.Br(),
                dash_tabulator.DashTabulator(
                        id      = 'activities_tabulator',
                        columns = activities_cols,
                        theme   = 'tabulator_simple',
                        options = {'pagination': 'local',
                                   'paginationSize': 200, 'height': 630},
                        data    = activities.to_dict('records'),
                        clearFilterButtonType = {"css": "btn btn-outline-dark", "text":"Effacer les filtres"}
                    ),
            ])
        ])
    ]) 

    # Tab content: 'Statistiques'
    stats_content = dbc.Card([
        dbc.CardBody([   
            
            html.H5('Lieux visités'), html.Br(),
            dbc.Row([
                dbc.Col([
                    html.Img(src=app.get_asset_url('summet_icon.jpg'), height='75px'),
                    html.Br(),
                    html.Label(f"Sommets: {stats_places['Sommet']}"),
                ], width={"size": 2, "offset": 1}),
                dbc.Col([
                    html.Img(src=app.get_asset_url('col_icon.jpg'), height='75px'),
                    html.Br(),
                    html.Label(f"Cols: {stats_places['Col']}"),
                ], width = 2),
                dbc.Col([
                    html.Img(src=app.get_asset_url('POI_icon.jpg'), height='75px'),
                    html.Br(),
                    html.Label(f"POI: {stats_places['POI']}"),
                ], width = 2),                   
                dbc.Col([
                    html.Img(src=app.get_asset_url('hut_icon.jpg'), height='75px'),
                    html.Br(),
                    html.Label(f"Refuges: {stats_places['Refuge']}"),
                ], width = 2),
                dbc.Col([
                    html.Img(src=app.get_asset_url('falaise_icon.jpg'), height='75px'),
                    html.Br(),
                    html.Label(f"Falaises: {stats_places['Falaise']}"),
                ], width = 2),                   
            ]),
            
            dcc.Graph(id = 'all_waypoints'), # updated in callback
            dbc.RadioItems( options = [ {"label": item, "value": item} for item in ['Par année (incomplet)', 'Tri par altitude']],
                            value   = 'Par année (incomplet)',
                            id      = "switch_waypoints_fig",
                            inline = True),
            html.Br(), html.Hr(), 

            html.H5('Statistiques par année'),
            dcc.Graph(id='year_stats_figure'),
            dbc.RadioItems( options = [ {"label": item, "value": item} for item in ['Journées', 'Lieux (incomplet)']],
                value   = "Journées",                
                id      = "switch_year_stats",
                inline = True),
            html.Br(), html.Hr(),   

            dbc.Row([
                html.H5('Sorties par niveau'),
                dbc.Col([
                    dcc.Graph(figure=figures.climb_by_grades['Alpinisme']),
                ], width=6),
                dbc.Col([
                    dcc.Graph(figure=figures.climb_by_grades['Randonnée']),
                ], width=6),         
                dbc.Col([
                    dcc.Graph(figure=figures.climb_by_grades['Ski']),
                ], width=6),  
                dbc.Col([
                    dcc.Graph(figure=figures.climb_by_grades['Escalade']),
                ], width=6),                                              
            ]),
            
            html.Hr(),
            html.H5('Evolution du contexte'),
            dcc.Graph(figure=figures.context_evolution),

        ])
    ])     

    modal = dbc.Modal([
        dbc.ModalHeader(dbc.ModalTitle(id='modal_title')),
        dbc.ModalBody([
            html.Label(id='modal_label')
        ]),
        dbc.ModalFooter()
        ], id="modal", is_open=False)

    # Main layout
    layout = dbc.Card([
        dbc.CardBody([          
            dbc.Row([
                dbc.Col([
                    html.Img(src=app.get_asset_url('logo.png'), height='110px'),
                    html.Hr(),
                    dcc.Markdown("*Because it's there*")
                ], width=1),

                dbc.Col([
                    dbc.Tabs([
                        dbc.Tab(map_content, label="Carte"),
                        dbc.Tab(activities_content, label="Sorties"),
                        dbc.Tab(stats_content, label="Statistiques"),
                    ]),
                ], width=11),
            ]),
        ]),  
        dcc.Markdown('[Nicolas Dupuis](https://gitlab.com/nicolasdupuis/swissmountaingoat), powered by Dash and Python.'), 
        modal
    ])     

    return layout


#-------------------------------------------------------------------------------------------
# Callbacks 
#-------------------------------------------------------------------------------------------

# Update Map & figure when filters are updated
#----------------------------------------------------------------------------
@app.callback( [Output('map',                 'figure'),
                Output('all_waypoints',       'figure')],
               [Input('altitude_slider',      'value' ),
                Input('year_slider',          'value' ),                
                Input('type_selection',       'value' ),
                Input('search_place',         'value' ),
                Input('switch_waypoints_fig', 'value' )],
               [State('altitude_slider',      'min'   ),
                State('altitude_slider',      'max'   ), 
                State('year_slider',          'min'   ),
                State('year_slider',          'max'   )]
    )
def update_theMap(altitude_values, year_values, types, place, switch, min_alt, max_alt, min_year, max_year):

    if switch=='Tri par altitude':
        figure = figures.update_waypoints_figure(altitude_values, types)

    elif switch=='Par année (incomplet)':
        figure = figures.update_waypoints_year_figure(altitude_values, types)

    return (generate_map(figures, place, 
                         altitude_values, [min_alt, max_alt], 
                         year_values, [min_year, max_year],
                         types),
            figure)


# Update Year stats figures on switch
#----------------------------------------------------------------------------
@app.callback(  Output('year_stats_figure', 'figure'),
               [Input('switch_year_stats',  'value' )]
    )
def update_year_figure(switch):

    if switch=='Journées':
        figure = figures.activities_overtime

    elif switch=='Lieux (incomplet)':
        figure = figures.waypoints_overtime

    return figure


# Display how many activities/days were selected
#----------------------------------------------------------------------------
@app.callback( Output('selection_sorties',    'children'),
               [Input('activities_tabulator', 'dataFiltered')]
            )
def n_activities_filtered(rows):

    if rows: 

        n = sum([1 for row in rows['rows'] if row != None])    
        days = sum([row['Journées'] for row in rows['rows'] if row != None])    

        return f"{n} activités affichées ({int(days)} journées)"


# Display filters
#----------------------------------------------------------------------------
@app.callback(
    Output("collapse_filters", "is_open"),
    [Input("filter_button",    "n_clicks")],
    [State("collapse_filters", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open



# Display modal form when user clicks on a waypoint in a graph
# @app.callback( [Output('modal',              'is_open'  ),
#                 Output('modal_title',        'children' ),
#                 Output('modal_label',        'children' )],
#                [Input('activities_overtime', 'clickData')]
#             )
# def open_modal(clickData):

#     print(clickData)

#     if clickData:
#         modal_title = f"Sortie(s) incluant '{clickData['points'][0]['hovertext']}'"
#         modal_label = 'blablabla'         
#         return (True, modal_title, modal_label)

#     else: 
#         return (False, '', '')

# ----------------------------------------------------------------------------
# Create the App
# ----------------------------------------------------------------------------
print(f"Reload at {datetime.now()}")

# Apply layout
app.layout = layout(figures)

# Here we go!
if __name__ == '__main__':
    app.run_server(debug=True)
